#!/bin/sh

# Ensure script is run as root
if [ "$(id -u)" != "0" ]; then
    echo "This script must be run as root. Exiting."
    exit 1
fi

echo "Thank you for using Gosh-Its-Arch FreeBSD system and package updater"

# Function to display a message and pause for 3 seconds
display_message() {
    echo "$1"
    sleep 3
}

# Fetch updates for the FreeBSD system
display_message "Fetching updates for the FreeBSD system..."
freebsd-update fetch

# Install the fetched updates
display_message "Installing fetched updates for the FreeBSD system..."
freebsd-update install

# Upgrade installed packages
display_message "Upgrading installed packages..."
pkg upgrade

echo "Update process complete."
