#!/bin/sh

# Display a welcome message
echo "Welcome to Gosh-Its-Arch KDE on FreeBSD installer"
sleep 3

# Install the necessary packages
echo "\nInstalling necessary KDE packages..."
sleep 3
pkg install --yes kde5 plasma5-sddm-kcm sddm xorg

# Enable and start the dbus service
echo "\nEnabling and starting the dbus service..."
sleep 3
sysrc dbus_enable="YES"
service dbus start

# Enable and start the sddm service
echo "\nEnabling and starting the sddm service..."
sleep 3
sysrc sddm_enable="YES"
service sddm start

echo "\nInstallation complete!"
exit 0
