# FreeBSD Scripts


## Getting started

These scripts are meant to make life easier when using FreeBSD

Remember you using this at your own risk. These are not official scripts please don't ask FreeBSD team for help.

Scripts must be run as root!

## Steps FreeBSD (64 bit)

Open up a terminal (be root) and run 

- git clone https://gitlab.com/gosh-its-arch-linux/freebsd.git
- cd freebsd
- chmod +x *.sh


Once completed you should be able to run the individual scripts (All must be run as root)

## What each file does

- update_system -> Use this script to automatically update FreeBSD

- setup_KDE -> Install KDE Desktop Enviroment from a base system. Also sets the symbolic links, so KDE boots up automatically.
